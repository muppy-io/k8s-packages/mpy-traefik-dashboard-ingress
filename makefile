# Définition des variables
#CC=gcc
#CFLAGS=-I.
#DEPS = # dépendances, par exemple: header.h
#OBJ = # fichiers objets générés à partir des fichiers source, par exemple: main.o module.o


VALUES_FILE ?= "values.yaml"
NAMESPACE=traefik-pack8s
CHART_NAME=mpy-traefik-dashboard-ingress
#HELM_CHART_NAME ?= "mpy.metapackage"
GITLAB_REGISTRY_USERNAME="${USER}"
GITLAB_REGISTRY_TOKEN="${MPY_REPO_GIT_TOKEN}"
GITLAB_PROJECT_ID=56390117

# Cible par défaut affichant l'aide
help:
	@echo "Available targets:"
	@echo "  build                                  - Update dependencies and build package"
	@echo "  publish                                - Deploy package on gitlab"
	@echo "  info                                   - Print package info (URL, repo)"
	@echo "  dbg_template [VALUES_FILE=values.yaml] - Just render templates"
	@echo "  dbg_install [VALUES_FILE=values.yaml]  - Install package in dry mode"
	@echo "  install [VALUES_FILE=values.yaml]      - Install package in namespace toctoc"
	@echo "  delete                                 - Delete package in namespace defined by NAMESPACE"	
	@echo "  help                                   - Display this message"
	@echo  ""
	@echo  "ENV Vars:"
	@echo  "  - VALUES_FILE=values.yaml"
	@echo  "  - NAMESPACE=traefik-pack8s"
	@echo  "  - CHART_NAME=mpy-traefik-dashboard-ingress"
	@echo  ""
# Cible pour construire le projet
build:
	helm dependency update .
	helm dependency build .
	helm package --dependency-update . --destination=pkg

dbg_template:
	helm template --debug ${CHART_NAME} . -f ${VALUES_FILE} > tests/helm-debug.txt

dbg_install:
	helm -n ${NAMESPACE} install --debug --dry-run ${CHART_NAME} . -f ${VALUES_FILE} > helm-debug.txt

install:
	helm -n ${NAMESPACE} install ${CHART_NAME} . -f ${VALUES_FILE}

delete:
	helm -n ${NAMESPACE} delete ${CHART_NAME}

# Cible pour déployer le package da
publish:
	@echo "Déploiement de l'application..."
	@sudo wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -O /usr/bin/yq && sudo chmod +x /usr/bin/yq
	@export MVBASE64="$$(base64 -w 0 values-muppy.yaml)" && yq eval '.annotations."muppy-values" = strenv(MVBASE64)' --inplace Chart.yaml
	@echo "GITLAB_REGISTRY_USERNAME=${GITLAB_REGISTRY_USERNAME}"
	@echo "GITLAB_REGISTRY_TOKEN=${GITLAB_REGISTRY_TOKEN}"
	export HELM_CHART_NAME=$$(basename $$(helm package --dependency-update . | awk '{print $$NF}')) && echo $$HELM_CHART_NAME ; \
	curl --request POST --form "chart=@$$HELM_CHART_NAME" --user ${GITLAB_REGISTRY_USERNAME}:${GITLAB_REGISTRY_TOKEN} \
    	https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/packages/helm/api/stable/charts ; \
	rm -f "$$HELM_CHART_NAME"

info:
	@echo ""
	@echo "Package URL =>  https://gitlab.com/muppy-io/k8s-packages/mpy-traefik-dashboard-ingress"
	@echo "Repo URL    =>  https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/packages/helm/stable"
	@echo ""

# Cible pour nettoyer le projet (supprime les fichiers objets et l'exécutable)
clean:
	rm -rf pkg/*
	rm -f Chart.lock

# Empêcher make de confondre les cibles sans dépendances avec des fichiers
.PHONY: help build deploy clean
