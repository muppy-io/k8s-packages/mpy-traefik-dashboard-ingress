# Traefik Deprecation warning

 * `traefik.containo.us/v1alpha1` must be replaced by `traefik.io/v1alpha1` starting Traefik 2.10
 * `traefik.containo.us/v1alpha1` and `traefik.io/v1alpha1` must be replaced by `traefik.io/v1` starting Traefik 3


# Build this package

```bash
# in chart folder

# This will rebuild dependencies
rm -rf charts
rm -rf Chart.lock
helm dependency build .

helm package --dependency-update . --destination=pkg

# refresh
helm dependency update .


# 
helm -n traefik-pack8s list
helm -n traefik-pack8s delete toctoc

helm -n traefik-pack8s install 
helm -n traefik-pack8s install --create-namespace --debug --dry-run traefik-dashboard-ingress . -f values.yaml 
```

# To publish the Helm Chart to Gitlab Registry

Adjust and commit all changes before.

```bash
cd utils
export HELM_CHART_NAME=$(basename $(helm package --dependency-update .. | awk '{print $NF}'))  && echo "chart name=${HELM_CHART_NAME}" 
export GITLAB_REGISTRY_USERNAME=cmorisse
export GITLAB_REGISTRY_TOKEN=$REPO_GIT_TOKEN
export GITLAB_PROJECT_ID=56390117
# To upload the package to gitlab registry
curl --request POST \
    --form "chart=@$HELM_CHART_NAME" \
    --user $GITLAB_REGISTRY_USERNAME:$GITLAB_REGISTRY_TOKEN \
    https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/packages/helm/api/stable/charts
rm $HELM_CHART_NAME
```

# To install the package from Gitlab Package Repository (For Dev)

helm repo add mpy-traefik-dashboard-ingress-charts https://gitlab.com/api/v4/projects/56390117/packages/helm/stable
helm repo update mpy-traefik-dashboard-ingress-charts

helm install one mpy-traefik-dashboard-ingress-charts/mpy-traefik-dashboard-ingress
```

